import logging
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils.rnn import pad_sequence

from transformers import BertModel, BertTokenizer, OpenAIGPTModel, OpenAIGPTTokenizer, GPT2Model, GPT2Tokenizer, CTRLModel, CTRLTokenizer, TransfoXLModel, TransfoXLTokenizer, XLNetModel, XLNetTokenizer, XLMModel, XLMTokenizer, DistilBertModel, DistilBertTokenizer, RobertaModel, RobertaTokenizer



def get_fine_tuned_bert(modelname):
    logging.info("Loading fine-tuned BERT...")
    bert = torch.load("{}/model_fine_tuned_bert".format(modelname))
    return bert



BERT_IDS={'bert-base-uncased',
          'bert-large-uncased',
          'bert-base-cased',
          'bert-large-cased',
          'bert-base-multilingual-uncased',
          'bert-base-multilingual-cased',
#          'bert-base-chinese',
          'bert-base-german-cased',
#          'bert-large-uncased-whole-word-masking',
#          'bert-large-cased-whole-word-masking',
#          'bert-large-uncased-whole-word-masking-finetuned-squad',
#          'bert-large-cased-whole-word-masking-finetuned-squad',
#          'bert-base-cased-finetuned-mrpc',
#          'bert-base-german-dbmdz-cased',
#          'bert-base-german-dbmdz-uncased'
}
DISTILBERT_IDS = {
    'distilbert-base-uncased',
    'distilbert-base-uncased-distilled-squad'
}

GPT2_IDS = {
    "gpt2",
    "gpt2-medium",
    "gpt2-large",
    "distilgpt2"
}




# Transformers has a unified API
# for 8 transformer architectures and 30 pretrained weights.
#          Model          | Tokenizer          | Pretrained weights shortcut
MODELS = [(BertModel,       BertTokenizer,       'bert-base-uncased'),
          (OpenAIGPTModel,  OpenAIGPTTokenizer,  'openai-gpt'),
          (GPT2Model,       GPT2Tokenizer,       'gpt2'),
          (CTRLModel,       CTRLTokenizer,       'ctrl'),
          (TransfoXLModel,  TransfoXLTokenizer,  'transfo-xl-wt103'),
          (XLNetModel,      XLNetTokenizer,      'xlnet-base-cased'),
          (XLMModel,        XLMTokenizer,        'xlm-mlm-enfr-1024'),
          (DistilBertModel, DistilBertTokenizer, 'distilbert-base-uncased'),
          (RobertaModel,    RobertaTokenizer,    'roberta-base')]

def get_model_tokenizer(model_id):
    if "distilbert" in model_id:
        return (DistilBertModel, DistilBertTokenizer)
    elif "bert" in model_id:
        return (BertModel, BertTokenizer)
    elif "gpt2" in model_id:
        return (GPT2Model, GPT2Tokenizer)


class RawEncoder(nn.Module):
    def __init__(self, bert_id):
        super(RawEncoder, self).__init__()
        model, tokenizer = get_model_tokenizer(bert_id)

        self.tokenizer = tokenizer.from_pretrained(bert_id, do_lower_case=("uncased" in bert_id))
        self.model = model.from_pretrained(bert_id)

    def encode_text(self, input_string):
        return torch.tensor([self.tokenizer.encode(input_string, add_special_tokens=True)])

    def forward(self, input_tensor):
        return self.model(input_tensor)[0]

class BertEncoder(nn.Module):

    def __init__(self, bert_id="bert-base-cased"):
        super(BertEncoder, self).__init__()
#        bert-base-uncased: 12-layer, 768-hidden, 12-heads, 110M parameters
#        bert-large-uncased: 24-layer, 1024-hidden, 16-heads, 340M parameters
#        bert-base-cased: 12-layer, 768-hidden, 12-heads , 110M parameters
#        bert-large-cased: 2
        model, tokenizer = get_model_tokenizer(bert_id)

        self.tokenizer = tokenizer.from_pretrained(bert_id, do_lower_case=("uncased" in bert_id))
        self.bert = model.from_pretrained(bert_id)

        # just to get the device easily
        self.device = nn.Parameter(torch.zeros(1))

        self.replace = {"-LRB-": "(", "-RRB-": ")"}

        self.distilled = "distil" in bert_id
        #self.dim = BERT_DIM


    def forward(self, sentence, batch=False):
        if batch:
            batch_tokens = []
            batch_masks = []
            batch_lengths = []
            for token_list in sentence:
                for i in range(len(token_list)):
                    if token_list[i] in self.replace:
                        token_list[i] = self.replace[token_list[i]]

                    wptokens = []
                    for token in token_list:
                        wp = self.tokenizer.wordpiece_tokenizer.tokenize(token)
                        wptokens.extend(wp)

                    mask = [0 if tok[:2] == "##" else 1 for tok in wptokens]
                    mask = torch.tensor(mask, dtype=torch.uint8, device=self.device.device)

                    indexed_tokens = self.tokenizer.convert_tokens_to_ids(wptokens)
                    tokens_tensor = torch.tensor([indexed_tokens], device = self.device.device)

                batch_tokens.append(tokens_tensor.view(-1))
                batch_masks.append(mask)
                batch_lengths.append(len(wptokens))
            
            padded = pad_sequence(batch_tokens, batch_first=True)
            mask = padded != 0
            encoded_layers,_ = self.bert(input_ids=padded, attention_mask=mask)
            split_layers = encoded_layers.split([1 for _ in sentence])
            assert(len(split_layers) == len(batch_masks))
            filtered_layers = [layer.squeeze(0)[:l][m] for layer, l, m in zip(split_layers, batch_lengths, batch_masks)]

            return filtered_layers
        else:
            for i in range(len(sentence)):
                if sentence[i] in self.replace:
                    sentence[i] = self.replace[sentence[i]]

            wptokens = []
            for token in sentence:
                wp = self.tokenizer.wordpiece_tokenizer.tokenize(token)
                wptokens.extend(wp)

            print(wptokens)
            mask = [0 if tok[:2] == "##" else 1 for tok in wptokens]
            mask = torch.tensor(mask, dtype=torch.uint8, device=self.device.device)

            indexed_tokens = self.tokenizer.convert_tokens_to_ids(wptokens)
            tokens_tensor = torch.tensor([indexed_tokens], device = self.device.device)

            if self.distilled:
                encoded_layers = self.bert(tokens_tensor)
                filtered_layers = encoded_layers[0].squeeze(0)[mask]
                return filtered_layers
            else:
                encoded_layers,_ = self.bert(tokens_tensor)
                filtered_layers = encoded_layers.squeeze(0)[mask]
                return filtered_layers


if __name__ == "__main__":
    for bertid in list(BERT_IDS) + list(DISTILBERT_IDS) + list(GPT2_IDS):
        bert = RawEncoder(bertid)
        print(bertid)
        sentence = "The bill ,would prevent the Resolution Trust Corp. from raising temporary working capital ."
    
        tensor = bert.encode_text(sentence)
        output = bert(tensor)
        print(output.shape)




#if __name__ == "__main__":
#    for bertid in list(BERT_IDS) + list(DISTILBERT_IDS) + list(GPT2_IDS):
#        bert = BertEncoder(bertid)
#        print(bertid)
#        #bert.cuda()

#        #sentence = "The bill , whose backers include Chairman Dan Rostenkowski -LRB- D. , Ill. -RRB- , would prevent the Resolution Trust Corp. from raising temporary working capital by having an RTC-owned bank or thrift issue debt that would n't be counted on the federal budget ."

#        sentence = "The bill ,would prevent the Resolution Trust Corp. from raising temporary working capital ."
#        if "uncased" in bertid:
#            sentence = sentence.lower()
#        sentence = sentence.split()

#        print(len(sentence))
#        output = bert(sentence, batch=False)
#        print([o.shape for o in output])
#        print(len(output))
#        print()



